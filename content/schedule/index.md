+++
categories = []
date = "2016-08-16T22:56:20-04:00"
tags = []
title = "Schedule"

+++

The schedule posted below is subject to change. The <i class="fa fa-hourglass-end"></i>
icon indicates an assignment is due that week.

> Unless otherwise specified, assignments are due on ~~Mondays~~ **Wednesdays**
> at 11:59 PM AOE. For example, "Lab 1" is due on **Wednesday, September 7 at
> 11:59 PM AOE**, which is the same as **Thursday, September 8 at 7:59 AM**
> Eastern US time. Similarly, for exceptional due dates, assume 11:59 pm AOE.


Date          | Topics
--------------|-------------------------------------------------------
Aug 22-24     | Setup: Jupyter 101 + [Python review](http://www.cc.gatech.edu/~simpkins/teaching/python-bootcamp/august2016.html))
              |     <i class="fa fa-hourglass-end"></i> Lab 0
Aug 29-31     | Association mining, sparse matrices (1), data collection (requests web API)
Sep 5-7       | **(No class Sep 5: Labor Day)** Preprocessing: Unstructured files, regular expressions (Enron email archive)
              |     <i class="fa fa-hourglass-end"></i> Lab 1
Sep 12-14     | Data collection (requests module and web APIs)
              |     <i class="fa fa-hourglass-end"></i> **Mon Sep 12** Lab 2
Sep 19-21     | Tidy data
              |     <i class="fa fa-hourglass-end"></i> Lab 3
Sep 26-28     | Tidy data (continued) + data viz
              |     (no lab due)
Oct 3-5       | SQL
              |     <i class="fa fa-hourglass-end"></i> **Mon Oct 3, with free late pass** Lab 4
Oct 10-12     | **(No class this week)** Linear algebra review
              |     <i class="fa fa-hourglass-end"></i> Lab 5
Oct 17-19     | Numerical computing in Python (Numpy); PageRank
              |     <i class="fa fa-hourglass-end"></i> Lab 6
Oct 24-26     | Linear regression and K-means
              |     <i class="fa fa-hourglass-end"></i> Lab 7
Oct 31-Nov 2  | Functional programming (Intro to [R](http://r-project.org))
              |     <i class="fa fa-hourglass-end"></i> Lab 8
Nov 7-9       | Logistic regression
              |     <i class="fa fa-hourglass-end"></i> Lab 9
Nov 14-16     | **(No class; instructors on travel)**
              |     (no lab due)
Nov 21-23     | **(No class Nov 23: Thanksgiving)** Numerical optimization
              |     <i class="fa fa-hourglass-end"></i> Lab 10
Nov 28-30     | SVD and friends
              |     <i class="fa fa-hourglass-end"></i> Lab 11
Dec 5-7       | (special topics: TBD)
              |     <i class="fa fa-hourglass-end"></i> **Mon Dec 5 with free late pass** Lab 12
              |
Wed Dec 14    | <i class="fa fa-stop"></i> **Final Exam:** 2:50-5:40 pm in Klaus 2447


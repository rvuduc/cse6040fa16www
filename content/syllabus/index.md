+++
categories = []
date = "2016-08-15T17:55:37-04:00"
tags = []
title = "Syllabus"

+++

# CSE 6040: Intro to Computing for Data Analysis

**Fall 2016 instructors:** [Professor Richard (Rich) Vuduc](http://vuduc.org) and [Sara Karamati](https://www.linkedin.com/in/karamati)

> This course is listed in [OSCAR](https://oscar.gatech.edu/) as "[88728 - CSE 6040 - A](https://oscar.gatech.edu/pls/bprod/bwckschd.p_disp_detail_sched?term_in=201608&crn_in=88728)."

## Quick links ##

- T-Square: https://t-square.gatech.edu/portal/site/gtc-3bd6-e221-5b9f-b047-31c7564358b7
- Piazza discussion forums: http://piazza.com/gatech/fall2016/cse6040/home

## Description ##

This course is your hands-on introduction to basic programming techniques relevant to data analysis and machine learning. Beyond programming languages and best practices, you'll learn elementary data processing algorithms, numerical linear algebra, and numerical optimization. You will build the basic components of a data analysis pipeline: collection, preprocessing, storage, analysis, and visualization. You will program in some subset of Python, R, MATLAB, and SQL, at our discretion.

## Prerequisites ##

To register, you must be a student in Georgia Tech's [MS Analytics](http://analytics.gatech.edu) program.

This course aims to fill in gaps in your programming background, in preparation for other programming-intensive courses in the MS Analytics program. If you come to the program with a significant programming background already, you will probably get less out of this course than you might have otherwise.

The formal prerequisite for this course, as it appears in OSCAR, is:
`Undergraduate Semester level CS 1371 Minimum Grade of D`

In more human terms, you should be familiar with basic programming ideas at the level of the [Python Bootcamp](http://www.cc.gatech.edu/~simpkins/teaching/python-bootcamp/), which most MS Analytics students would have taken.

We also assume you have had undergraduate freshman or sophomore calculus, probability, and linear algebra.

## When & Where? ##

The class meets Mondays and Wednesdays from 4:30-6 pm in the [Klaus Advanced Computing Building (KACB or "Klaus")](https://goo.gl/maps/wkhUDy83pLE2), [Room 2447](http://www.oit.gatech.edu/service/classroom-support/room-details?roomid=358).

**Office hours.**

* Sara: **Mon** ~~2:30-3:30 pm~~ **1:30-2:30 pm** in Klaus 1335 (or, when the room is not available, the nearby alcove)
* Rich: ~~Wed 3:05-4:25 pm~~ **Tu 3:45-4:45 pm** in Klaus 1334

## School supplies ##

* Pencil, paper, and brain!
* Laptop with wifi, a modern web browser, and a Jupyter notebook installation for use in class
* There is no required textbook.

## Philosophy and approach ##

The basic philosophy of this course is that you'll learn the material best by a combination of reading, thinking, and most importantly, _actively doing_. Therefore, we will try to make lectures interactive. This approach will only work if you prepare _before_ class and participate _actively_ during class, whenever asked.

## Assignments and grading ##

Your grade will be based on a combination of in-class labs and a final exam.

- In-class labs: 70% (each lab is weighted equally)
- Final exam: 30%

An in-class lab will be due on most weeks according to the posted schedule.

**Due date convention.** For all assignments with due dates, a posted due date should be interpreted as “23:59 anywhere on earth” (11:59 pm AOE). For example, if an assignment is due on August 31, as long as there is any place on the planet Earth where it is 11:59 pm or earlier, your submission is on time. (You are responsible for taking signal transmission delays into account, especially if you are connecting from outer space.)

> For the ultimate in precision timing, here’s a handy AOE clock: http://www.timeanddate.com/time/zones/aoe

**Late policy.** You get three "late passes." That is, for any milestone, you may submit the assignment up to 48 hours after the official due date without penalty. Any assignment submitted after you run out of passes or after 48 hours (with or without a pass) will get zero credit.

**Collaboration policy.** All Georgia Tech students are expected to uphold the Georgia Tech Academic Honor Code. Honest and ethical behavior is expected at all times. All incidents of suspected dishonesty will be reported to and handled by the Dean of Students office. Penalties for violating the collaboration policy can be severe; alleged violations are adjudicated by the Dean of Students office and not by the instructor.

You may collaborate on assignments at the "whiteboard" level. That is, you can discuss ideas and have technical conversations with other students in the class, which we especially encourage on the Piazza forums. However, each student must write-up and submit his or her own reports and code.


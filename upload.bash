#!/usr/bin/env bash

set -x
rm -rf public/ && hugo && rsync -avz -e ssh public/. cse6040:httpdocs/fa16/.

# eof
